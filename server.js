const yargs = require('yargs/yargs')
const { hideBin } = require('yargs/helpers')
const colors = require('colors')
const dgram = require('dgram')
const fs = require('fs')
const path = require('path')
const os = require('os')

const clientsFilePath = path.join(__dirname, './clients.json')

// Get registered clients
let registeredClients = JSON.parse(fs.readFileSync(clientsFilePath, { encoding: 'utf8' }))

// Create UDP4 (IPv4) dgram socket
const server = dgram.createSocket('udp4')

// Required const
const serverPort = 7777

// Command indicator
let clearClients = false // false by default

// Other vars
let serverMode = 'discovery' // discovery by default
let applicationName = '' // Empty by default
let clientPort = null // Null by default
let remoteData = {} // Empty object by default
let remoteAddress = '' // Empty string by default
const broadcastAddresses = []
let broadcastLimit = 20 // 20 by default

console.log((`
-------------------------------------------
MyStation UDP Discovery/Remote server
-------------------------------------------`).cyan)

console.log('Made with ' + (('❤').red) + '  by ' + 'Hervé Perchec <herve.perchec@gmail.com>'.yellow)
console.log('See also ' + `http://mystation.fr/`.yellow)
console.log((`-------------------------------------------\n`).cyan)

// Get argv
var y = yargs(hideBin(process.argv))
  .command('clear-clients', 'Clear registered clients', () => {}, (argv) => {
    clearClients = true
  })
  .command('serve', 'Start the server')
  .option('mode', {
    alias: 'm',
    type: 'string',
    description: 'Define the server mode'
  })
  .option('application', {
    alias: 'a',
    type: 'string',
    description: 'Define the application to discover or control'
  })
  .option('port', {
    alias: 'p',
    type: 'string',
    description: 'Define the target client port'
  })
  .option('broadcast-limit', {
    type: 'string',
    description: 'Set limit of tries for broadcast requests (only use with "discovery" mode)'
  })
  .option('data', {
    alias: 'd',
    type: 'string',
    description: 'Set data to send in remote mode (only use with "remote" mode)'
  })
  .option('address', {
    type: 'string',
    description: 'Set address of client to send in remote mode (only use with "remote" mode)'
  })
  .demandCommand()
  .help()
  .argv

// If command clear-clients
if (clearClients) {
  // Reset registered clients
  registeredClients = []
  fs.writeFileSync(clientsFilePath, JSON.stringify(registeredClients, null, 4))
  console.info(`Discovery Server: ` + (`CLEARING`.magenta) + ` - Clear registered clients in ${clientsFilePath}`)
  process.exit(0)
}

// If --mode option defined
if (y.mode) {
  // must be "discovery" or "remote"
  if (y.mode === 'discovery' || y.mode === 'remote') {
    serverMode = y.mode
  // Else, error -> unknown type
  } else {
    console.error('Discovery Server: ' + ('ERROR'.red) + ' - unknown server mode')
    throw new Error('Unknown server mode (must be "discovery" or "remote")')
  }
}

// If --application option defined
if (y.application) {
  applicationName = y.application
} else {
  console.error('Discovery Server: ' + ('ERROR'.red) + ' - --application option must be defined')
  throw new Error('Please fill the "application" option...')
}

// If --port option defined
if (y.port) {
  clientPort = y.port
} else {
  console.error('Discovery Server: ' + ('ERROR'.red) + ' - --port option must be defined')
  throw new Error('Please fill the client "port" option...')
}

// If --broadcast-limit option defined
if (y.broadcastLimit) {
  if (!isNaN(y.broadcastLimit)) {
    broadcastLimit = y.broadcastLimit
  } else {
    console.error('Discovery Server: ' + ('ERROR'.red) + ' - --broadcast-limit option must be a number > 0')
    throw new Error('Please enter a number for the broadcast limit option...')
  }
}

// If remote mode
if (serverMode == 'remote') {

  // If --data option defined
  if (y.data) {
    if (JSON.parse(y.data)) {
      remoteData = JSON.parse(y.data)
    } else {
      console.error('Remote Server: ' + ('ERROR'.red) + ' - --data option must be in JSON format')
      throw new Error('Please give valid JSON data')
    }
  } else {
    console.error('Remote Server: ' + ('ERROR'.red) + ' - --data option missing')
    throw new Error('Please give valid JSON data')
  }

  // If --address option defined
  if (y.address) {
    remoteAddress = y.address
  } else {
    console.error('Remote Server: ' + ('ERROR'.red) + ' - --address option missing')
    throw new Error('Please give valid client IP address')
  }

}

// Get server network interfaces
const interfaces = os.networkInterfaces()
const serverInterfaces = []
// For each interfaces
for (let i in interfaces) {
  // Parse data
  for (let data of interfaces[i]) {
    // Ignore other than ipv4
    if (data.family !== 'IPv4') continue
    // Ignore localhost 127.0.0.1
    if (data.address === '127.0.0.1') continue
    // Else -> get address and netmask
    const address = data.address.split('.').map(e => parseInt(e))
    const netmask = data.netmask.split('.').map(e => parseInt(e))
    // Push address to serverInterfaces
    serverInterfaces.push({ address: address, port: serverPort })
    // Push network address to tmp array
    const networkAddress = address.map((e, i) => (~netmask[i] & 0xff) | e).join('.')
    broadcastAddresses.push(networkAddress)
  }
} 

// Body of the message sent
const messageBody = {
  mode: serverMode,
  server: {
    name: 'MyStation',
    network: serverInterfaces
  },
  data: {}
}

// Go
const run = function () {

  var interval = null

  /**
   * Send message to each broadcast address
   * @return {void}
   */
  function broadcast () {
    let broadcastRequests = 1
    interval = setInterval(() => {
      // For each broadcast addresses -> send message: message (Buffer), target port, IP address
      broadcastAddresses.forEach((address) => {
        console.log(`Discovery Server: ${('BROADCAST'.blue)} - (${broadcastRequests}/${broadcastLimit}) [${address}:${clientPort}]`)
        // Send message
        server.send(Buffer.from(JSON.stringify(
          {
            ...messageBody,
            data: {
              application: applicationName
            }
          })),
          clientPort,
          address
        )
      })
      // Increment for broadcast limit
      if ( broadcastRequests < broadcastLimit ) {
        broadcastRequests++
      } else {
        stop()
        // Exit after 2 secondes (broadcast requests are every 1s)
        setTimeout(function () { 
          process.exit(0)
        }, 2000)
      }
    }, 1000)
  }

  /**
   * Stop: stop request interval
   * @return {void}
   */
  function stop () {
    // Clear interval
    clearInterval(interval)
    console.log(`Discovery Server: ${('STOPPING'.yellow)} - Stopping broadcast requests (${broadcastLimit}/${broadcastLimit})`)
  }

  // Error event
  server.on('error', err => {
    console.error('Discovery Server: ' + ('ERROR'.red) + ' - ', err)
    server.close()
  })

  // Listening event -> emitted when server.bind()
  server.on('listening', () => {
    // Activate broadcast
    server.setBroadcast(true)
    console.log('Discovery Server: ' + ('STARTED'.green) + ' - Listening on port ' + serverPort)
  })

  // Message event
  server.on('message', (msg, rinfo) => {
    let JSONmsg
    // Try to parse JSON message
    try {
      JSONmsg = JSON.parse(msg.toString('utf8'))
    } catch (err) {
      console.error(`Discovery Server: ` + (`ERROR`.red) + ` - message received from ${rinfo.address}:${rinfo.port} not in readable JSON format.`)
      return
    }
    // Check application name
    if (JSONmsg.client.application !== applicationName) {
      console.error(`Discovery Server: ` + (`IGNORE`.yellow) + ` - Message received from ${rinfo.address}:${rinfo.port} : don't match application name.`)
      return
    }

    // Check mode
    if (JSONmsg.mode == 'discovery') {
      console.info(`Discovery Server: ` + (`RECEIVED RESPONSE`.cyan) + ` - Response from ${rinfo.address}:${rinfo.port}`)
      // In case of 'discovery' mode, check if client already exists and save client
      const found = registeredClients.find(element => element.application == JSONmsg.client.application && element.port == rinfo.port && element.key == JSONmsg.client.key)
      if (!found) {
        registeredClients.push({
          address: rinfo.address,
          port: rinfo.port,
          application: JSONmsg.client.application,
          key: JSONmsg.client.key
        })
        fs.writeFileSync(clientsFilePath, JSON.stringify(registeredClients, null, 4))
        console.info(`Discovery Server: ` + (`CLIENT ADDED`.cyan) + ` - ${rinfo.address} (Application: ${applicationName})`)
      }
      console.info(`Discovery Server: ` + (`EXISTING CLIENT`.yellow) + ` - Skipping...`)
    } else if (JSONmsg.mode == 'remote') {
      // Remote control
      // To do
      // Retour du remote à prendre en charge
      console.log(`Discovery Server: [RESPONSE_DATA] ${JSON.stringify(JSONmsg)} [/RESPONSE_DATA]`)
      process.exit(0)
    } else {
      console.error(`Discovery Server: ` + (`IGNORE`.yellow) + ` - Message received from ${rinfo.address}:${rinfo.port} : unknown mode (${JSONmsg.mode}).`)
      return
    }
  })

  server.bind(serverPort, () => {
    if (serverMode === 'discovery') {
      broadcast()
    } else if (serverMode === 'remote') {
      console.info(`Discovery Server: ` + (`REMOTE`.cyan) + ` - Remote request send to ${remoteAddress}:${clientPort} (Application: ${applicationName}, data: ${JSON.stringify(remoteData)})`)
      // Send message
      server.send(Buffer.from(JSON.stringify(
        {
          ...messageBody,
          data: {
            application: applicationName,
            ...remoteData
          }
        })),
        clientPort,
        remoteAddress
      )
      // Timed out -> 10s
      setTimeout(function () { 
        console.error(`Discovery Server: ` + (`ERROR`.red) + ` - Timed out... Request aborted after 10 seconds.`)
        process.exit(1) // Exit with error
      }, 10000)
    }
  })
  
}

run()
