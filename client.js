const dgram = require('dgram')
const childp = require('child_process')
const fs = require('fs')
const path = require('path')
const os = require('os')
const colors = require('colors')

// Get controller KEY
const KEY = fs.readFileSync(path.join(__dirname, '../key'), { encoding: 'utf8' })

// Create UDP4 (IPv4) dgram socket
const client = dgram.createSocket('udp4')

const loggerLabel = '[SMART GARDEN]'
const clientPort = 7701
const applicationName = 'SMART_GARDEN'

// Body of the message sent
const messageBody = {
  mode: 'discovery', // discovery by default
  client: {
    name: 'Smart Garden controller',
    application: applicationName,
    key: KEY
  },
  data: {}
}

const run = function () {

  // Error event
  client.on('error', err => {
    console.error(loggerLabel + ' client: ' + ('ERROR'.red) + ' - ', err)
    client.close()
  });
  // Listening event -> emitted when client.bind()
  client.on('listening', () => {
    console.log(loggerLabel + ' client: ' + ('STARTED'.green) + ' [' + client.address().address + ':' + clientPort + ']')
  })
  // Message event
  client.on('message', (msg, rinfo) => {
    let JSONmsg
    // Try to parse JSON message
    try {
      JSONmsg = JSON.parse(msg.toString('utf8'))
    } catch (err) {
      console.error(`${loggerLabel} client: ` + (`ERROR`.red) + ` message received from ${rinfo.address}:${rinfo.port} not in readable JSON format.`)
      return
    }
    // Check application name
    if (JSONmsg.data.application !== applicationName) {
      console.error(`${loggerLabel} client: ` + (`IGNORE`.yellow) + ` message received from ${rinfo.address}:${rinfo.port} : don't match application name.`)
      return
    }
    // Check mode
    if (JSONmsg.mode == 'discovery') {
      console.info(`${loggerLabel} client: ` + (`RECEIVED MESSAGE`.cyan) + ` received discovery message from ${rinfo.address}:${rinfo.port}`)
      console.info(`${loggerLabel} client: ` + (`SEND RESPONSE`.cyan) + ` to ${rinfo.address}:${rinfo.port}`)
      // In case of 'discovery' mode, send network info to server
      client.send(Buffer.from(JSON.stringify(
        {
          ...messageBody,
          mode: 'discovery',
          data: {
            response: 'success'
          }
        })),
        rinfo.port,
        rinfo.address
      )
    } else if (JSONmsg.mode == 'remote') {
      console.info(`${loggerLabel} client: ` + (`RECEIVED MESSAGE`.cyan) + ` received remote message from ${rinfo.address}:${rinfo.port}`)
      // Check output
      if (JSONmsg.data.output) {
        // If number and between 1 and 4
        if (!isNaN(JSONmsg.data.output) && JSONmsg.data.output > 0 && JSONmsg.data.output < 5) {
          // Check status
          if (JSONmsg.data.status) {
            // If 'on' or 'off'
            if (JSONmsg.data.status == 'on' || JSONmsg.data.status == 'off') {
              // Ok, everything is good, return response to server
              childp.execSync(`python ~/smart-garden/gpio-controller/remote.py ${JSONmsg.data.output} ${JSONmsg.data.status}`, { stdio: 'inherit' })
              // Send Ok to server
              client.send(Buffer.from(JSON.stringify(
                {
                  ...messageBody,
                  mode: 'remote',
                  data: {
                    response: 'success',
                    output: JSONmsg.data.output,
                    status: JSONmsg.data.status
                  }
                })),
                rinfo.port,
                rinfo.address
              )
            } else {
              console.error(`${loggerLabel} client: ` + (`IGNORE`.yellow) + ` (remote mode) invalid value for: "status". Must be 'on' or 'off'`)
              return
            }
          } else {
            console.error(`${loggerLabel} client: ` + (`IGNORE`.yellow) + ` (remote mode) missing data: "status"`)
            return
          }
        } else {
          console.error(`${loggerLabel} client: ` + (`IGNORE`.yellow) + ` (remote mode) invalid type data for: "output". Must be number between 1 and 4`)
          return
        }
      } else {
        console.error(`${loggerLabel} client: ` + (`IGNORE`.yellow) + ` (remote mode) missing data: "output"`)
        return
      }
    } else {
      console.error(`${loggerLabel} client: ` + (`IGNORE`.yellow) + ` message received from ${rinfo.address}:${rinfo.port} : unknown mode (${JSONmsg.mode}).`)
      return
    }

  })
  // Bind
  client.bind(clientPort)
}

run()